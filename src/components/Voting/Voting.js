import './Voting.css'
import BackButton from "../BackButton/BackButton";
import {useEffect, useState} from "react";
import Send from "../Send/Send";

const Voting = () => {
    const [boys, setBoys] = useState(null)

    useEffect(() => {
        const answer = Send('get', `/getPair`)
        setBoys(JSON.parse(answer))
    }, []);

    const handleOnCLick = (number) => {
        Send('get', `/vote?dogId=${boys[number].id}`)
        const answer = Send('get', `/getPair`)
        setBoys(JSON.parse(answer))
    }

    return (
        <div>
            <BackButton />
            <div className={'votingContainer'}>
                <div className={'votingItem'} style={{backgroundImage: boys && `url(${boys[0].image})`}}>
                    <div className={'votingItemInside'} onClick={() => handleOnCLick(0)}>
                        Этот мальчик лучше!
                    </div>
                </div>
                <div className={'votingItem'} style={{backgroundImage: boys && `url(${boys[1].image})`}}>
                    <div className={'votingItemInside'} onClick={() => handleOnCLick(1)}>
                        Этот мальчик лучше!
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Voting;
