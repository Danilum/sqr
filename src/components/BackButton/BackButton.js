import './BackButton.css'
import {NavLink} from "react-router-dom";

const BackButton = () => {
    return (
        <NavLink to={"../"} className={'BackButton'}>
            &lt;
        </NavLink>
    );
}

export default BackButton;
