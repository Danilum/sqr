import './Background.css'
import MainContainer from "../MainContainer/MainContainer";

const Background = () => {
    return (
        <div className={'background'}>
            <MainContainer />
        </div>
    );
}

export default Background;
