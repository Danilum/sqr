import './Search.css'
import BackButton from "../BackButton/BackButton";
import { useParams } from "react-router-dom";
import CustomButton from "../CustomButton/CustomButton";
import Send from "../Send/Send";
import {useEffect, useState} from "react";

const Search = () => {
    const { breed } = useParams()
    const [boy, setBoy] = useState(null)

    useEffect(async () => {
        const answer = await Send('get', `/find?breedName=${breed}`)
        setBoy(JSON.parse(answer))
    }, []);

    const handleOnCLick = () => {
        Send('post', `/addToFight`, boy) // , { breed }
    }

    return (
        <div className={'SearchMainContainer'}>
            <BackButton />
            <div className={'SearchImage'} style={{backgroundImage: boy && `url(${boy.image})`}} />
            <br />
            <CustomButton ButtonText={'Добавить к голосованию'} onClick={handleOnCLick}/>
        </div>
    );
}

export default Search;
