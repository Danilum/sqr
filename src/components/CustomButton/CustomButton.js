import './CustomButton.css'

const CustomButton = ({ButtonText, onClick}) => {
    return (
        <div className={'CustomButton'} onClick={onClick}>
            {ButtonText}
        </div>
    );
}

export default CustomButton;
